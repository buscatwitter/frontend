# Frontend

Neste repositório temos o código fonte referente a aplicação para o usuário final

**Tecnologias**

-  Vue.js
-  NodeJS
-  NPM
-  Docker

## Pré-requisitos

- Plataforma NodeJS / VueJS
- Gerenciador de Pacotes NPM
- Ambiente Docker

## Instalação

``` bash
# instalar dependências
npm install
```

## Execução

``` bash
# Ambiente local - localhost:8080
npm run dev

# Ambiente produtivo
npm run build
```

## Geração de Container Docker

```bash
docker build -t frontend_casetwitter .
```


