import Cadastro from './components/cadastro/Cadastro.vue';
import Home from './components/home/Home.vue';

export const routes = [
    { path: '', component: Home, titulo: 'Top Usuarios'},
    { path: '/cadastro', component: Cadastro, titulo: 'Pesquisa de Hashtags'}
];