export default class Util {

    static getURLAPI() {
        return (process.env.URL_API) ? process.env.URL_API : 'http://localhost:3000';
      }    
}
